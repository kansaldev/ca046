#include<stdio.h>

void DispTrans(int a[30][30],int n)
{ printf("\nTHE MATRIX: \n");
  for(int i=0;i<n;i++)
   { for(int j=0;j<n;j++)
     { printf("%d\t",a[i][j]);}
     printf("\n"); }
  
  printf("\nTHE TRANSPOSE: \n");
  for(int i=0;i<n;i++)
   { for(int j=0;j<n;j++)
      printf("%d\t",a[j][i]);
     printf("\n");  }
}

void getarray(int n)
{ printf("Enter the elements of the array:\n");
  int a[30][30];
  for(int i=0;i<n;i++)
   for(int j=0;j<n;j++)
    scanf("%d",&a[i][j]);
   
   DispTrans(a,n);
}

int main()
{ int n;
  printf("Enetr the no of rows and columns for the square matrix :");
  scanf("%d",&n);
  getarray(n);
  return 0;
  }

