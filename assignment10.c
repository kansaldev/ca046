#include<stdio.h>

int search(int a[],int n,int e)
{ int flag=0;
  for(int i=0;i<n;i++)
   if(a[i]==e)
    flag++;
  return flag;
 }

int* getarray(int n)
{ int a[n];
  printf("Enter the array: \n");
  for(int i=0;i<n;i++)
   scanf("%d",&a[i]);
  
  return a;
  }

void display(int flag)
{ if(flag)
   printf("\nThe element is present in the array.");
  else
   printf("\nThe element is not present in the array.");
  }
int main()
{ int n,flag,e;
  int* a;
  printf("Enter the no of elements in the array: ");
  scanf("%d",&n);
  a=getarray(n);
  printf("\nEnter the element whose presence is to be found: ");
  scanf("%d",&e);
  flag=search(a,n,e);
  display(flag);
  return 0;
  }