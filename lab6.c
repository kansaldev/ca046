#include<stdio.h>

int getno()
{ int n;
  printf("Enter the no of elements in array");
  scanf("%d",&n);
  return  n;
  }
  
int* getarray(int n)
{ int a[n];
  printf("Enter the the elements of the array :\n");
  for(int i=0;i<n;i++)
   scanf("%d",&a[i]);
  return a;
  }

int average(int a[],int n)
{ int sum=0; 
  for(int i=0;i<n;i++)
    sum=sum+a[i];
  return sum/n;
  }

int main()
{ int n,avg;
  int* a;
  n=getno();
  a=getarray(n);
  avg=average(a,n);
  printf("The average of the elements in the array : %d",avg);
  return 0;
  }