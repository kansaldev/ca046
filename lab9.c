#include<stdio.h>
struct student
{
    int roll;
    char name[50];
    char section[10];
    char dept[50];
    float fees;
    float result;
};
void input(struct student *s1)
{
    printf("\n Enter the name of student : ");
    fflush(stdin);
    gets(s1->;name);
    printf("\n Enter the section of student : ");
    fflush(stdin);
    gets(s1->section);
    printf("\n Enter the department of student : ");
    fflush(stdin);
    gets(s1->dept);
    printf("\n Enter the roll number of student : ");
    scanf("%d", &s1->roll);
    printf("\n Enter the fees of student : ");
    scanf("%f", &s1->fees);
    printf("\n Enter the result of student : ");
    scanf("%f", &s1->result);
}
int compute(float a, float b)
{
    int flag=0;
    if(a > b)

    flag = 1;
    else
    flag = 2;
    return flag;
}
void display(struct student s1, struct student s2, int flag)
{
    printf("\n Student results are as follows : \n");
    printf("\n\t STUDENT 1 \n Roll number: %d \n Name: %s \n Section: %s \n Dept: %s \n Fees: %f \n Result: %.2f", s1.roll, s1.name, s1.section, s1.dept,s1.fees, s1.result);
    printf("\n\t STUDENT 2 \n Roll number: %d \n Name: %s \n Section: %s \nDept: %s \n Fees: %f \n Result: %.2f", s2.roll, s2.name, s2.section, s2.dept,s2.fees, s2.result);
    if(flag == 1)
    printf("\n\n Student 1 scored higher than student 2, with %.2f",s1.result);
    else
    printf("\n\n Student 2 scored higher than student 1, with %.2f",s2.result);
}
    
int main()
{
    struct student s1, s2;
    int flag=0;
    input(&s1);
    input(&s2);
    flag = compute(s1.result, s2.result);
    display(s1, s2, flag);
    return 0;
}