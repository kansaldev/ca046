#include<stdio.h>
void input(int *a, int *b)
{
    printf("\n Enter two numbers : ");
    scanf("%d", a);
    scanf("%d", b);
}
void add(int a, int b, int *sum)

{
    *sum = a + b;
}
void subtract(int a, int b, int *diff)
{
    *diff = a - b;
}
void multiply(int a, int b, int *product)
{
    *product = a * b;
}
void divide(int a, int b, int *quotient)
{
    *quotient = a / b;
}
void rdr(int a, int b, int *rem)
{
    *rem = a%b;
}
void display(int a, int b, int c, int d, int e)
{
    printf("\n Sum = %d", a);
    printf("\n Difference = %d ", b);
    printf("\n Product = %d", c);
    printf("\n Quotient = %d ", d);
    printf("\n Remainder = %d ", e);
}

int main()
{
    int a, b;
    int sum=0, diff=0, prod=0, quo=0;
    int rem=0;
    input(&a,&b);
    add(a, b, &sum);
    subtract(a, b, &diff);
    multiply(a, b, &prod);
    divide(a, b, &quo);
    rdr(a, b, &rem);
    display(sum, diff, prod, quo, rem);
    return 0;
}