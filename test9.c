#include<stdio.h>
void input(int *a, int *b)
{
    printf("\n Enter a number : ");
    scanf("%d",a);
    printf("\n Enter another number : ");
    scanf("%d",b);
}
void swap(int *a, int *b)
{

    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}
void display(int a, int b)
{
    printf("\n a = %d", a);
    printf("\n b = %d", b);
}
int main()
{
    int a, b;
    input(&a,&b);
    printf("\n Before swapping : ");
    display(a,b);
    printf("\n After swapping : ");
    swap(&a, &b);
    display(a,b);
    return 0;
}

